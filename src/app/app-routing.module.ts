import { Routes } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuardService } from '../app/shared/services/auth-guard.service'

export const Approutes: Routes = [

  {
    path: '',
    redirectTo: 'dashboard/dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuardService],
  },
  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'dashboard',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super','manager','finance' ,'operation', 'dispatcher','generic']
    },
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      }
    ]
  },

  {
    path: 'starter',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super', 'manager', 'finance' ,'operation']
    },
    children: [
      // { path: '', redirectTo: '/starter', pathMatch: 'full' },
      {
        path: 'starter',
        loadChildren: './starter/starter.module#StarterModule'
      },
      {
        path: 'component',
        loadChildren: './component/component.module#ComponentsModule'
      },
      {
        path: 'views',
        loadChildren: './views/views.module#ViewsModule'
      }
    ]
  },
  {
    path: 'settings',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super']
    },
    children: [
      { path: '', redirectTo: '/settings', pathMatch: 'full' },
      {
        path: 'vehiclecategory',
        loadChildren: './settings/settings.module#SettingsModule'
      }
    ]
  },
  {
    path: 'vehicles',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super', 'manager' ,'operation']
    },
    children: [
      { path: '', redirectTo: '/vehicles', pathMatch: 'full' },
      {
        path: 'managevehicle',
        loadChildren: './vehicles/vehicle.module#VehicleModule'
      }
    ]
  },
  {
    path: 'cooparate',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super', 'manager', 'finance']
    },
    children: [
      { path: '', redirectTo: '/cooparate', pathMatch: 'full' },
      {
        path: 'cooparateUsers',
        loadChildren: './cooparate/cooparate.module#CooparateModule'
      }
    ]
  },
  {
    path: 'cooparateUsers',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super', 'manager', 'finance']
    },
    children: [
      { path: '', redirectTo: '/cooparateUsers', pathMatch: 'full' },
      {
        path: 'cooparate-Users',
        loadChildren: './cooparate-users/cooparate-users.module#CooparateUsersModule'
      }
    ]
  },
  {
    path: 'trips',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super', 'finance', 'dispatcher' ,'operation']
    },
    children: [
      { path: '', redirectTo: '/trips', pathMatch: 'full' },
      {
        path: 'trips',
        loadChildren: './trips/trips.module#TripsModule'
      }
    ]
  },
  {
    path: 'dispatcher',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super', 'finance']
    },
    children: [
      { path: '', redirectTo: '/dispatcher', pathMatch: 'full' },
      {
        path: 'dispatcher',
        loadChildren: './dispatcher/dispatcher.module#DispatcherModule'
      }
    ]
  },
  {
    path: 'wallet',
    component: FullComponent,
    canActivate: [AuthGuardService],
    data: { 
      expectedRole: ['super','finance' ]
    },
    children: [
      { path: '', redirectTo: '/wallet', pathMatch: 'full' },
      {
        path: 'wallet',
        loadChildren: './wallet/wallet.module#WalletModule'
      }
    ]
  }
];
