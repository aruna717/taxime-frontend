import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApprovedVehiclesComponent } from './approved-vehicles/approved-vehicles.component';
import { Routes, RouterModule } from '@angular/router';
import { PendingVehiclesComponent } from './pending-vehicles/pending-vehicles.component';


export const VehicleRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'approvedvehicles',
        component: ApprovedVehiclesComponent,
        data: {
          title: 'Approved Vehicles',
          urls: [
            { title: 'Approved Vehicles', url: '/approvedvehicles' }
          ]
        }
      },
      {
        path: 'pendingapprovelvehicles',
        component: PendingVehiclesComponent,
        data: {
          title: 'Pending Approvel Vehicles',
          urls: [
            { title: 'Pending Approvel Vehicles', url: '/pendingapprovelvehicles' }
          ]
        }
      }
    ]
  }
];