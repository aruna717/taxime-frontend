import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

let httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'JWT ' + localStorage.getItem('usertoken')
  })
}

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  uri = environment.apiBase

  constructor(private _http: HttpClient) { }

  getCompanyWallet(from, to) {
    var fromUTC = from
  var toUTC = to
    return this._http.get<any>(this.uri + 'admin/getCompanyWallet/'+ fromUTC+'/'+toUTC, httpOptions);
  }

  getMoreData(id, type) {
    return this._http.get<any>(this.uri + 'admin/gettripDataByTripId/'+ id +'/'+ type, httpOptions);
  }
}
