import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../shared/services/login.service'
import { TokenPayload } from '../../shared/model/tokenPayload'
import { Injectable } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { from } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  password: string = "";
  username: string = "";

  constructor(
    public router: Router,
    public loginService: LoginService,
    private snotifyService: SnotifyService
  ) { }

  ngOnInit() {
  }

  loginform = true;
  recoverform = false;

  showRecoverForm() {
    this.loginform = !this.loginform;
    this.recoverform = !this.recoverform;
  }
 
  login() {
    var credentials: TokenPayload = {
      email: this.username,
      password: this.password
    }
    this.loginService.login(credentials).subscribe(
      (data) => {
        // console.log(data)
        this.router.navigateByUrl('dashboard/dashboard')

      },
      err => {
        // if(err)
        this.error(err.error.details);
        console.error(err)
      }
    )
  }

  success(msg){
    this.snotifyService.success(msg, {
      timeout: 1000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      position: "rightTop"
    })
  }

  error(msg){
    this.snotifyService.error(msg, {
      timeout: 2000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      position: "rightTop"
    })
  }
  
  info(msg){
    this.snotifyService.info(msg, {
      timeout: 2000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      position: "rightTop"
    })
  }
}
