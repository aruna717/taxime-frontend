import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { CompanyWalletComponent } from './company-wallet/company-wallet.component';


export const WalletRoutingModule: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CompanyWalletComponent,
        data: {
          title: 'CompanyWallet',
          urls: [
            { title: 'CompanyWallet', url: '/companywallet' }
          ]
        }
      }
    ]
  }
]
