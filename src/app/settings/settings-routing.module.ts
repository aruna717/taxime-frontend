import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehicleCategoriesComponent } from './vehicle-categories/vehicle-categories.component';
import { Routes, RouterModule } from '@angular/router';


export const SettingsRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'vehiclecategories',
        component: VehicleCategoriesComponent,
        data: {
          title: 'Vehicle Categories',
          urls: [
            { title: 'Peding Approvals Drivers', url: '/vehiclecategories' }
          ]
        }
      }
    ]
  }
];

