import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment'
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmDirectionModule } from 'agm-direction';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatCardModule } from '@angular/material/card';
import { TripsRoutingModule } from './trips-routing.module';
import { DispatchComponent } from './dispatch/dispatch.component';
// import { PlacePredictionService } from '../shared/services/map/place-prediction.service'
import { MatButtonModule } from '@angular/material/button';
import { from } from 'rxjs';
import { SnotifyModule, ToastDefaults, SnotifyService } from 'ng-snotify';
import { HistoryComponent } from './history/history.component';
import { DataTablesModule } from 'angular-datatables';
import { DispatchHistoryComponent } from './dispatch-history/dispatch-history.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    SnotifyModule,
    RouterModule.forChild(TripsRoutingModule),
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapApiKey,
      libraries: ["places,geometry,"],
      apiVersion: '4'
    }),
    AgmDirectionModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    NgxSpinnerModule,
    DataTablesModule,
    NgxPaginationModule
  ],
  declarations: [
    DispatchComponent,
    HistoryComponent,
    DispatchHistoryComponent
  ],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
    SnotifyService
  ],
})
export class TripsModule { }
