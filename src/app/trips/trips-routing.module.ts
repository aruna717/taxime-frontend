import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { DispatchComponent } from './dispatch/dispatch.component';
import { from } from 'rxjs';
import { HistoryComponent } from './history/history.component';
import { DispatchHistoryComponent } from './dispatch-history/dispatch-history.component';


export const TripsRoutingModule: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dispatch',
        component: DispatchComponent,
        data: {
          title: 'Dispatch',
          urls: [
            { title: 'Dispatch', url: '/dispatch' }
          ]
        }
      }
    ]
  },
  {
    path: 'history',
    component: HistoryComponent,
    data: {
      title: 'History',
      urls: [
        { title: 'History', url: '/history' }
      ]
    }
  },
  {
    path: 'dispatchHistory',
    component: DispatchHistoryComponent,
    data: {
      title: 'Dispatch History',
      urls: [
        { title: 'Dispatch History', url: '/dispatchHistory' }
      ]
    }
  }
]