// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  apiBase : 'https://connect.snaplk.com/',
  ws_url : 'https://adminsocket.snaplk.com/',
  imgApiBase : 'https://connect.snaplk.com/',
  googleMapApiKey: 'AIzaSyAZPcxlwexVBUDpBX6OgenSBIH3IteLD6A',
  production: false

  // // // test 
  // apiBase : 'http://35.192.65.121:8085/',
  // ws_url : 'http://35.192.65.121:8087/',
  // imgApiBase : 'http://35.192.65.121:8085/',
  // googleMapApiKey: 'AIzaSyAZPcxlwexVBUDpBX6OgenSBIH3IteLD6A',
  // production: false
};
